const express = require('express');
const bodyParser =  require('body-parser');
const exphbs = require('express-handlebars');
const path = require('path');
const nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
const app = express();

//view engine setup 
app.engine('handlebars',exphbs({extname:'handlebars',defaultLayout:null}));
app.set('view engine','handlebars');

//static folder
app.use('/public', express.static(path.join(__dirname,'public')));

//body parser middleware
//parse application/x-www-form-urlencoded

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.get('/',(req,res)=> {
    res.render("contact")
});

app.post('/send',(req,res)=>{
    const output = `
        <p>You have a new contact request</p>
        <h3>Contact Details</h3>
        <ul>
        <li>Name : ${req.body.name}</li>
        <li>Company : ${req.body.company}</li>
        <li>Email : ${req.body.email}</li>
        <li>Phone : ${req.body.phone}</li>
        </ul>
        <h3><p>${req.body.message}</p></h3>
    `;
    var transporter = nodemailer.createTransport(smtpTransport({
        service: 'gmail',
        host: 'smtp.gmail.com',
        auth: {
          user: 'chakka.yamini1999@gmail.com',
          pass: '9848665011'
        }
      }));
    
      // send mail with defined transport object
     var mailOptions = {
        from: '"Mail Contact 👻" <chakka.yamini1999@gmail.com>', // sender address
        to: "shoyabshaik96@gmail.com", // list of receivers
        subject: "Node contact using nodemailer", // Subject line
        text: "Hello world?", // plain text body
        html: output, // html body
      };

      transporter.sendMail(mailOptions,(error,info)=>{
          if(error)
          {
              return console.log(error);
          }
          console.log("Message sent: %s", info.messageId);
      // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    
      // Preview only available when sending through an Ethereal account
      console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
      // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
      res.render('contact',{msg:'Email has been sent'});
      });
    });
app.listen(9000,()=>console.log(`Server started`));