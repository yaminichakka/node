var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  let dataArray = [
    {
      name:'Yamini',
      age:21
    },
    {
      name:'Shravya',
      age:21
    },
    {
      name:'Priyanka',
      age:21
    }
  ];
  res.json({
    data:dataArray
  })
});

module.exports = router;
