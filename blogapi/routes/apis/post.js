const express = require('express');
const router=express.Router();
const Post = require('../../models/Post')

//get request
router.get('/',(req,res,next)=>{
    Post.find()
    .then((posts)=>{
        res.json(posts);
    })
    .catch(err => console.log(err))
});

//post request
router.post('/add',(req,res,next)=>{
    const title= req.body.title;
    const body = req.body.body;
    newPost = new Post({
        title:title,
        body:body
    });
    newPost.save()
    .then(post=>{
        res.json(post);
    })
    .catch(err => console.log(err));
})

//update request

router.put('/update/:id',(req,res,next)=>{
    //grab id from params
    let id = req.params.id;
    //FInd the post by id from database
     Post.findById(id)
    .then(post => {
        post.title = req.body.title;
        post.body = req.body.body;
        post.save()
        .then(post =>{
            res.send({message:'Post Updated Successfully',status:'success',post:post})
        })
        .catch(err => console.log(err))
    })
    .catch(err => console.log(err))
});

//delete request
router.delete('/:id',(req,res,next)=>{
    let id = req.params.id;
    Post.findById(id)
    .then(post => {
        post.delete()
        .then(post =>{
            res.send({message:'Deleted Successfully',status:'success',post:post})
        })
        .catch(err => console.log(err))
    })
    .catch(err => console.log(err))
});
module.exports = router;