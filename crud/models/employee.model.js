const mongoose = require('mongoose');
var employeeSchema = new mongoose.Schema({
    FullName:
    {
        type:String,
        required:'This field is required'
    },
    Email:
    {
        type:String
    },
    MobileNumber:
    {
        type:Number
    },
    City:
    {
        type:String
    }
});

mongoose.model('Employee',employeeSchema);